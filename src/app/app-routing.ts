import { Routes, RouterModule } from '@angular/router';
import { EmailList } from './emailComponents/email-list.component';
import { EmailDetails } from './emailComponents/email-details.component';
import { Main } from './main/main.component'

const appRoutes: Routes = [

    {
        path: '', component: Main},
       
            { path: ':id', component: EmailDetails }
       
   
];

export const routing = RouterModule.forRoot(appRoutes);
