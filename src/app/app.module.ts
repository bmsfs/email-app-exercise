import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { AppComponent } from './app.component';
import { HttpModule } from '@angular/http';
//email components
import { EmailList } from './emailComponents/email-list.component';
import { EmailDetails } from './emailComponents/email-details.component';
//service for calling api and get emails
import { EmailService } from './_services/emailService';
//component for routing
import { routing } from './app-routing'
//main that holds the mail inbox (outside appComponent, in order to be able to provide authentication)
import { Main } from './main/main.component';
import { HttpClientModule } from '@angular/common/http';

@NgModule({
  declarations: [
    AppComponent,
    EmailList,
    EmailDetails,
    Main,
  ],
  imports: [
    BrowserModule,
    routing,
    HttpClientModule
  ],
  exports: [
    RouterModule
  ],
  providers: [
    EmailService,
    HttpModule
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
