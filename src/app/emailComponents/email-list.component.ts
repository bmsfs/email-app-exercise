import { Component } from '@angular/core';
import { EmailService } from '../_services/emailService';
import { HttpModule } from '@angular/http';

 
@Component({
  selector: 'email-list',
  templateUrl: 'email-list.html'
})
export class EmailList {

  emails=[];
  pageinfo:any={};
  subjects=[];
constructor(
  private api:EmailService,
){}
ngOnInit(){
  this.api.getEmails().subscribe(
    (data:any) => {
      this.emails = data[0].collection.items;
      this.pageinfo = data[0].collection.pageinfo;
      this.subjects = data[0].collection.items.subjects
      },
 error=>{})
}
}