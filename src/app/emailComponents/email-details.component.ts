import { Component } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { EmailService } from '../_services/emailService';

@Component({
  selector: 'email-details',
  templateUrl: 'email-details.html',
  styles: ['.mailcontainer {max-height:300px;overflow-y:scroll;margin-bottom:20px;}']

})
export class EmailDetails {
  emailNr: any;
  name:string;
  mailBody=[];
  mailSubject=[];
  toggle:string = 'html';
  constructor(
    public route: ActivatedRoute,
    private api: EmailService
  ) {
    //gets the id number from the routing
    this.route.params.subscribe(params => { this.emailNr = params.id });
  }
  ngOnInit() {
    this.api.getEmailbyId(this.emailNr).subscribe(
      (data: any) => {
        //maybe should map to a single object, but since this is just an exercise with no form...
        this.name = data[0].name;
        this.mailBody = data[0].body;
        this.mailSubject = data[0].subjects
      },
      error => { })
  }
  //toggles between html or plain
  toggles(param) {
    this.toggle = param;
    return;
  }
}










