import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';




@Injectable()
export class EmailService {

    constructor(private http: HttpClient) { }

    getEmails(): Observable<any> {
    //calling api from another domain may cause some cors issues
        const headers = new HttpHeaders()
        .set('Access-Control-Allow-Origin:*',null);

        var r = this.http
            .get('http://5b60bffbbde36b00140812c1.mockapi.io/api/emailInbox/emails', { headers: headers });
        r.subscribe(
            (data: any) => { },
            (error: any) => { }
        );
        return r;
    }
    getEmailbyId(id): Observable<any> {
        //calling api from another domain may cause some cors issues
            const headers = new HttpHeaders()
            .set('Access-Control-Allow-Origin:*',null);
            var r = this.http
                .get('http://5b60bffbbde36b00140812c1.mockapi.io/api/emailInbox/email' + id , { headers: headers });
            r.subscribe(
                (data: any) => { },
                (error: any) => { }
            );
            return r;
        }
}